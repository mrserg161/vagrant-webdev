pathDomain="/domain/"                             # Путь к директории с вируальными хостами
pathApacheConf="/etc/apache2/sites-available/"    # Путь к директории Apache2
ServerAdmin="mrSerg161@gmail.com"
vmip=192.168.1.57

listDirDomain=`find /domain -maxdepth 1 -type d 2>/dev/null| awk '{FS="/"} {print $3}' | tr '\n' ' '`

for domain in $listDirDomain
do
  rm $pathApacheConf""$domain
  echo "---"$domain"---"
  echo " # ## Virtual host "$domain" ## #
  <VirtualHost *:80>
  ServerName "$domain"
  ServerAlias www."$domain"
  ServerAdmin "$ServerAdmin"
  DocumentRoot "$pathDomain""$domain"
  ErrorLog /var/log/apache2/"$domain".error.log
  CustomLog /var/log/apache2/"$domain".access.log combined
  <Directory "$pathDomain""$domain">
    AllowOverride All
  </Directory>
  </VirtualHost>" >> $pathApacheConf""$domain
  echo "virtual host conf file create!"

  # vhConf=${domain##*/}
  # регистрируем хосты
  # sudo a2ensite ${vhConf}
  # vhost=${vhConf%.*}

  # Добавляем запись в /etc/hosts  
  if grep -q "$domain" /etc/hosts;
    then
      echo "Запись в /etc/hosts уже существует"
    else
      sudo sed -i "2i${vmip}    ${domain}" /etc/hosts
      echo "Добавлена запись в /etc/hosts" 
  fi
  echo ""
done

# выставляем права и перезапускаем apache
sudo chmod -R 755 $pathDomain
sudo service apache2 restart

# Устанавливаем mc:
# sudo apt-get --assume-yes install mc

# Если потребуется обновить node/npm:
# sudo npm cache clean -f
# sudo npm install -g n
# sudo n stable